{
  // 猫头鹰控件处理
  const pwd = document.querySelector("#login [name=password]");
  pwd.addEventListener("focus", function () {
    this.parentNode.parentNode.previousElementSibling.classList.add("password");
  });

  pwd.addEventListener("blur", function () {
    this.parentNode.parentNode.previousElementSibling.classList.remove(
      "password"
    );
  });

  const pwd1 = document.querySelector("#register [name=password]");
  pwd1.addEventListener("focus", function () {
    this.parentNode.parentNode.previousElementSibling.classList.add("password");
  });

  pwd1.addEventListener("blur", function () {
    this.parentNode.parentNode.previousElementSibling.classList.remove(
      "password"
    );
  });
}

{
  // 切换注册
  const reg = document.querySelector("#login .register");
  reg.addEventListener("click", () => {
    document.querySelector("#login").style.display = "none";
    document.querySelector("#register").style.display = "block";
  });
  // 返回登录
  const login = document.querySelector("#register .register");
  login.addEventListener("click", () => {
    document.querySelector("#login").style.display = "block";
    document.querySelector("#register").style.display = "none";
  });
}
